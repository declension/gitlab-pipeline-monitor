module Config exposing (httpTimeout, maxBuildsPerBranch, maxNonDefaultBranches, maxPipelinesPerProject, maxProjects)


maxNonDefaultBranches =
    3


{-| We double to allow for some branches being massively more popular than others
and thus using up most of the "most recent builds" allowance.
-}
maxPipelinesPerProject =
    2 * maxBuildsPerBranch * (maxNonDefaultBranches + 1)


maxBuildsPerBranch =
    5


maxProjects =
    10


httpTimeout =
    10.0
