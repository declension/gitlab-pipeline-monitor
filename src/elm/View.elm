module View exposing (iconFor, maybeViewOauthLink, view)

import Browser
import Config exposing (maxBuildsPerBranch, maxNonDefaultBranches)
import Dict exposing (Dict)
import Html exposing (Html, a, div, footer, h2, h3, li, main_, ol, span, text)
import Html.Attributes exposing (class, classList, href, id, target)
import Model exposing (Flags, GitRef, Host, Model, Msg, Namespace, Pipeline, PipelineStore, Project, ProjectId, Status(..), UrlString)
import UnicodeEmoji exposing (unicodifyEmoji)
import Url exposing (Protocol(..), Url)
import Wire exposing (authUrlFor)



-- VIEW


view : Model -> Browser.Document msg
view model =
    { title = "Pipelines for " ++ model.config.gitlabHost ++ " - Gitlab Pipeline Monitor"
    , body = viewMain model
    }


viewMain : Model -> List (Html msg)
viewMain model =
    let
        wrap content =
            [ main_ [ class ("pg-" ++ model.url.path) ]
                content
            ]

        middle =
            case model.token of
                Nothing ->
                    maybeViewOauthLink model

                Just _ ->
                    if List.isEmpty model.data.projects then
                        [ div [ id "full-screen", class "loading" ] [ h2 [] [ text "Loading..." ] ] ]

                    else
                        [ div [ id "wrapper" ] (viewProjects model) ]

        bottom =
            [ footer [] [ div [] [ text "© 2020. ", a [ href "https://gitlab.com/declension/gitlab-pipeline-monitor" ] [ text "On gitlab.com" ] ] ] ]
    in
    wrap middle ++ bottom


viewProjects : Model -> List (Html msg)
viewProjects model =
    List.map (viewProjectFromPipelinesData model.data.pipelines) model.data.projects


viewProjectFromPipelinesData : Dict ProjectId (List Pipeline) -> Project -> Html msg
viewProjectFromPipelinesData allPipelines project =
    let
        pipelinesByGitRef =
            Dict.get project.id allPipelines
                |> Maybe.map byGitRef
                |> Maybe.withDefault Dict.empty
    in
    viewProject pipelinesByGitRef project


byGitRef : List Pipeline -> Dict GitRef (List Pipeline)
byGitRef pipelines =
    pipelines
        |> List.map (\p -> ( p.ref, p ))
        |> List.foldl addItem Dict.empty


addItem : ( GitRef, Pipeline ) -> Dict GitRef (List Pipeline) -> Dict GitRef (List Pipeline)
addItem ( gitRef, pipeline ) cur =
    Dict.update gitRef (appendItem pipeline) cur


appendItem : a -> Maybe (List a) -> Maybe (List a)
appendItem item maybeExistingList =
    maybeExistingList
        |> Maybe.map (\existing -> Just (item :: existing))
        |> Maybe.withDefault (Just [ item ])


viewProject : Dict GitRef (List Pipeline) -> Project -> Html msg
viewProject pipelines project =
    div [ class "project" ]
        ([ a [] [ text project.namespace ]
         , a [ href project.url, target "_blank" ]
            [ h2 [] [ text (unicodifyEmoji project.name) ]
            ]
         ]
            ++ maybeDescription project.description
            ++ [ viewProjectPipelines project pipelines ]
        )


maybeDescription : Maybe String -> List (Html msg)
maybeDescription maybeDesc =
    maybeDesc
        |> Maybe.map (\str -> [ div [ class "description" ] [ text (unicodifyEmoji str) ] ])
        |> Maybe.withDefault []


viewProjectPipelines : Project -> Dict GitRef (List Pipeline) -> Html msg
viewProjectPipelines project pipelineGroups =
    if Dict.isEmpty pipelineGroups then
        div [ class "empty" ] [ text "😴" ]

    else
        let
            masterGroup =
                Dict.get "master" pipelineGroups |> Maybe.withDefault []

            others =
                Dict.remove "master" pipelineGroups
                    |> Dict.toList
                    |> List.sortWith latestIdComparer
                    |> List.take maxNonDefaultBranches
                    |> List.map (viewPipelineGroup project.url)
        in
        ol [ class "pipeline-groups" ]
            (viewPipelineGroup project.url ( "master", masterGroup ) :: others)


latestIdComparer : ( a, List { b | id : Int } ) -> ( a, List { b | id : Int } ) -> Order
latestIdComparer ( _, leftBuilds ) ( _, rightBuilds ) =
    let
        getLatestId =
            List.map .id >> List.maximum >> Maybe.withDefault 0
    in
    compare (getLatestId rightBuilds) (getLatestId leftBuilds)


maybeViewOauthLink : Model -> List (Html msg)
maybeViewOauthLink model =
    case model.token of
        Nothing ->
            [ div [ id "full-screen", class "loading" ]
                [ a [ href <| Url.toString <| authUrlFor model.config model.url ]
                    [ h2 [] [ text "Authorising with Gitlab..." ]
                    ]
                ]
            ]

        _ ->
            []


viewPipelineGroup : UrlString -> ( GitRef, List Pipeline ) -> Html msg
viewPipelineGroup baseUrl ( gitRef, pipelines ) =
    let
        -- We could do: ++ "/tree/" ++ gitRef to link to code directly but seems less useful
        targetUrl =
            baseUrl ++ "/-/branches/all" ++ "?search=" ++ gitRef
    in
    li [ classList [ ( "master", gitRef == "master" ), ( "group", True ) ] ]
        [ h3 []
            [ a [ href targetUrl, target "_blank" ]
                [ text gitRef ]
            ]
        , div [ class "pipelines" ]
            (pipelines |> List.reverse |> List.take maxBuildsPerBranch |> List.map pipelineButtonOf)
        ]


pipelineButtonOf : Pipeline -> Html msg
pipelineButtonOf content =
    a [ class "pipeline", href content.url, target "_blank", class <| classFor content.status ]
        [ span [ class "emoji" ] [ text <| iconFor content.status ]
        ]


classFor status =
    case status of
        Success ->
            "success"

        Failed ->
            "failed"

        Running ->
            "running"

        Cancelled ->
            "failed"

        _ ->
            "unknown"


iconFor status =
    case status of
        Created ->
            "👶"

        Pending ->
            "😴"

        Success ->
            "😀"

        Failed ->
            "😟"

        Running ->
            "⏳"

        Cancelled ->
            "\u{1F92F}"

        Skipped ->
            "😶"
