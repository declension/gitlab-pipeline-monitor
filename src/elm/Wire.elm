module Wire exposing (QueryItem, authUrlFor, blankable, emptyHttps, encodeQueryPair, extractToken, fromQuery, getUrl, pipelineDecoder, pipelineStatusDecoder, pipelinesDecoder, pipelinesUrl, projectDecoder, projectsDecoder, projectsUrl, queryFor, toMaybeTuple, toToken)

import Config exposing (httpTimeout, maxPipelinesPerProject, maxProjects)
import Http exposing (emptyBody, expectJson, header)
import Iso8601
import Json.Decode as D exposing (Decoder)
import Model exposing (Flags, Host, Msg(..), Pipeline, Project, ProjectId, Status(..), Token)
import Url exposing (Protocol(..), Url)
import Url.Builder as Builder exposing (toQuery)
import Url.Parser exposing (query)
import Url.Parser.Query as Query
import Utils exposing (stripQuestion)


getUrl : Token -> Url -> (Result Http.Error a -> Msg) -> Decoder a -> Cmd Msg
getUrl token url msg decoder =
    Http.request
        { method = "GET"
        , url = Url.toString url
        , body = emptyBody
        , expect = expectJson msg decoder
        , headers = [ header "Authorization" ("Bearer " ++ token) ]
        , timeout = Just (httpTimeout * 1000.0)
        , tracker = Nothing
        }


pipelinesUrl : String -> ProjectId -> Url
pipelinesUrl host projectId =
    { emptyHttps
        | host = host
        , path = "/api/v4/projects/" ++ String.fromInt projectId ++ "/pipelines"
        , query = [ Builder.int "per_page" maxPipelinesPerProject, Builder.string "order_by" "id", Builder.string "sort" "desc" ] |> toQuery |> stripQuestion |> Just
    }


projectsUrl : Host -> Url
projectsUrl host =
    { emptyHttps
        | host = host
        , path = "/api/v4/projects/"
        , query =
            [ Builder.int "per_page" maxProjects, Builder.int "archived" 0, Builder.int "membership" 1, Builder.string "order_by" "last_activity_at" ]
                |> toQuery
                |> stripQuestion
                |> Just
    }


emptyHttps : Url
emptyHttps =
    { protocol = Https, host = "", port_ = Nothing, path = "", query = Nothing, fragment = Nothing }


pipelinesDecoder : Decoder (List Pipeline)
pipelinesDecoder =
    D.list pipelineDecoder


pipelineDecoder : Decoder Pipeline
pipelineDecoder =
    D.map4 Pipeline
        (D.field "ref" D.string)
        (D.field "id" D.int)
        (D.field "status" pipelineStatusDecoder)
        (D.field "web_url" D.string)


pipelineStatusDecoder : Decoder Status
pipelineStatusDecoder =
    let
        conv val =
            case val of
                "created" ->
                    D.succeed Created

                "success" ->
                    D.succeed Success

                "failed" ->
                    D.succeed Failed

                "pending" ->
                    D.succeed Pending

                "running" ->
                    D.succeed Running

                "canceled" ->
                    D.succeed Cancelled

                "skipped" ->
                    D.succeed Skipped

                _ ->
                    D.fail "Unknown status"
    in
    D.string |> D.andThen conv


projectsDecoder : Decoder (List Project)
projectsDecoder =
    D.list projectDecoder


projectDecoder : Decoder Project
projectDecoder =
    D.map6 Project
        (D.field "id" D.int)
        (D.at [ "namespace", "full_path" ] D.string)
        (D.field "name" D.string)
        (D.field "description" blankable)
        (D.field "web_url" D.string)
        (D.field "last_activity_at" <| Iso8601.decoder)


blankable : Decoder (Maybe String)
blankable =
    D.string
        |> D.nullable
        |> D.map
            (\str ->
                case str of
                    Just "" ->
                        Nothing

                    dunno ->
                        dunno
            )


extractToken : Url -> Maybe Token
extractToken url =
    url.fragment
        |> Maybe.map (String.append "http://DUMMY?")
        |> Maybe.andThen Url.fromString
        |> Maybe.andThen (Url.Parser.parse (query toToken))
        |> Maybe.withDefault Nothing


toToken : Query.Parser (Maybe Token)
toToken =
    Query.string "access_token"


{-| Create a URL suitable for clicking on to authenticate
-}
authUrlFor : Flags -> Url -> Url
authUrlFor config currentUrl =
    let
        currentQuery =
            currentUrl.query

        newQuery =
            currentQuery
                |> Maybe.map fromQuery
                -- If there's no query, let's still add this
                |> Maybe.withDefault []
                |> (::) ( "redirect", Nothing )
                |> queryFor

        redirectUrl =
            { currentUrl | query = Just newQuery }
    in
    { protocol = Https
    , host = config.gitlabHost
    , port_ = Nothing
    , path = Builder.absolute [ "oauth", "authorize" ] []
    , query =
        queryFor
            [ ( "client_id", Just config.gitlabAppId )
            , ( "response_type", Just "token" )

            -- TODO: inject state and persist
            , ( "state", Just "1234" )
            , ( "redirect_uri", Url.toString redirectUrl |> Just )
            ]
            |> Just
    , fragment = Nothing
    }


type alias QueryItem =
    ( String, Maybe String )


queryFor : List QueryItem -> String
queryFor =
    List.map encodeQueryPair
        >> String.join "&"


fromQuery : String -> List QueryItem
fromQuery str =
    str
        |> String.split "&"
        |> List.map (String.split "=")
        |> List.map (List.map Url.percentDecode)
        |> List.map toMaybeTuple
        |> List.filterMap identity


toMaybeTuple : List (Maybe a) -> Maybe ( a, Maybe a )
toMaybeTuple items =
    case items of
        [ Just k, Just v ] ->
            Just ( k, Just v )

        [ Just k ] ->
            Just ( k, Nothing )

        _ ->
            Nothing


{-| Encodes a key and a potential value as a query string.
Having `Maybe String` as the value allows us to
do things like <http://example.com/foo?redirect>
-}
encodeQueryPair : QueryItem -> String
encodeQueryPair ( key, maybeValue ) =
    Url.percentEncode key
        ++ (case maybeValue of
                Nothing ->
                    ""

                Just value ->
                    "=" ++ Url.percentEncode value
           )
