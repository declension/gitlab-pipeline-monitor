Gitlab Pipeline Monitor
=======================

![screenshot](./docs/screenshot-2020-03-05-declension.png)


What?
-----
A modern, dark-themed, big-screen-friendly UI for monitoring Gitlab Pipelines (CI) :grin:

Using the magic of [Elm](https://elm-lang.org/) and the [Gitlab v4 API](https://docs.gitlab.com/ee/api/).


Why?
----

### Aren't there already plenty of these?
Yes, but, err _this one's different_, etc etc.

### How so?

 * :nerd: Because: Elm is wonderful and JS, err, isn't so much. 
 * :closed_lock_with_key: Oh, it does use a semi-proper Oauth Implicit Flow, so no giving away your hard-earned API tokens.
 * :unicorn: But more importantly: judicious use of emojis everywhere 
 * :art: Lots of work on style and animations so that it looks nice (to me...)
 * :crystal_ball: Future ideas are easier in a new codebase (Canvas animations? WebGL shaders? 3D?)


How?
----

### Setting up a Gitlab Application

 * Go to <https://gitlab.com/profile/applications> (or your private equivalent)
 * Select `api` scope
 * Local: Add <http://localhost:1234?redirect> as a redirect URI
 * Deployed: any production / deployed URLs additionally e.g. <https://pipelines.example.com?redirect>
 * Create the app
 * Copy the application ID (you won't even need the secret :sparkles:)
 


### Configure your installation
 One way or another, configure the environment
 Easiest is by editing the [`.env` file](.env).
 
Make sure you override the things as documented there,
notably `GITLAB_APP_ID` from above.


### Build
```bash
yarn
```

### Run
1. Register a new app in your Gitlab UI e.g. https://gitlab.example.com/profile/applications
1. Make sure you fill a redirect URL for where you're planning to run this.
   The format is just the same URL you're deployed to, with a `redirect` query string (e.g. `http://localhost:1234?redirect`).
1. Edit the [`.env`](./.env) file in the root, and fill in the endpoint for your Gitlab hostname (assumes port 443 for now), the project ID to watch and the newly created Application ID.
1. Run the dev server (with HMR):
```
yarn serve
```


:information_source: *Wow, that was fast*
Yes! Elm 0.19 + Yarn + [ParcelJS](https://parceljs.org/) == :rocket: 


### Browse the UI
Go to your URL - e.g. <http://localhost:1234> if running locally with Parcel.


This now auto-redirects you to the auth page (if not check your URLs)
This should look a bit like this:
![OIDC confirmation](docs/gitlab-oidc-applicaiton-confirmation.png)
 
Click _Authorize_, and voilà, you should have the UI.


When?
-----

It's usable now, but still beta. Try it out!
We've been using it all day at $WORK for months without issue against a private Gitlab.

