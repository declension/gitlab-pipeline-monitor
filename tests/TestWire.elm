module TestWire exposing (suite)

import Expect
import Json.Decode as D
import Model exposing (Pipeline, Status(..))
import Test exposing (Test, describe, test)
import Wire exposing (fromQuery, pipelineDecoder, queryFor)


suite : Test
suite =
    describe "The Wire module"
        [ test "queryFor encodes key-values normally" <|
            \_ ->
                [ ( "foo bar", Just "bar baz" ) ]
                    |> queryFor
                    |> Expect.equal "foo%20bar=bar%20baz"
        , test "queryFor allows just keys" <|
            \_ ->
                [ ( "foo", Nothing ) ]
                    |> queryFor
                    |> Expect.equal "foo"
        , test "fromQuery decodes on key-values with %-encoding" <|
            \_ ->
                "foo=bar%20baz"
                    |> fromQuery
                    |> Expect.equal [ ( "foo", Just "bar baz" ) ]
        , test "decodes pipeline status of 'created' correctly" <|
            \_ ->
                """{"ref": "dummy-ref", "status": "created", "id": 12345, "web_url": "https://example.com/foobar" }"""
                    |> D.decodeString pipelineDecoder
                    |> Expect.equal (Ok (Pipeline "dummy-ref" 12345 Created "https://example.com/foobar"))
        ]



--         TODO: fuzzing FTW
